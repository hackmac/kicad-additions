# kicad-symbols

A local repository for storing KiCad library files which are needed by all my projects and which are not found at KiCad.


## License ![](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc-sa.eu.svg)

 [kicad-additions](https://bitbucket.org/hackmac/kicad-additions/) © 2018 by [Henrik Wedekind (aka. hackmac)](https://bitbucket.org/hackmac/) is licensed under [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/)
